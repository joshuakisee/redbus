<div 
style="background:transparent url('/CodeIgniter/images/hero.png') no-repeat center center /cover; width:100%;height:70%;">
<nav class="navbar navbar-default navbar-custom">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="/CodeIgniter/images/redbus.png" width="100%" height="100%"></a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       <ul class="nav navbar-nav">
        <a class="navbar-brand" href="#"><font color="#fff">BUS TICKETS</font></a>
        <a class="navbar-brand" href="#">BUS HIRE</a>
       </ul>
    

      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Print Ticket</a></li>
        <li><a href="#">Cancel Ticket/Track Refund</a></li>
        <li><a href="#">Conduct Us</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login/signup<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Login</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Sign up</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container">
<div class="row">
<div class="col-sm-12">

</div>
<div class="row">
	<div class="col-sm-12" align="center">
		<h2><font color="#fff">Online Bus Ticket Booking with Zero Booking Fees</font></h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-1">
	</div>
		<form>
			<div class="col-sm-2">
			<select name="from" class="form-control">
				<option>From</option>
			</select>
			</div>
			<div class="col-sm-2">
			<select name="to" class="form-control">
				<option>Destination</option>
			</select>
			</div>
			<div class="col-sm-2">
			<div class='input-group date' id='datetimepicker1'>
					<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar" ></span>
                    </span>
                    <input type='text' id = "datepicker-1" class="form-control" placeholder="ONWARD DATE" />
             </div>
			</div>
			<div class="col-sm-2">
			<div class='input-group date' id='datetimepicker1'>
				<span class="input-group-addon">
	                <span class="glyphicon glyphicon-calendar" ></span>
	            </span>
	            <input type='text' id = "datepicker-2" class="form-control" placeholder="RETURN DATE" />
             </div>
			</div>
			<div class="col-sm-2">
				<button class="form-control navbar-custom">Search Buses</button>
			</div>
		</form>
	<div class="col-sm-1">
	</div>
</div>
</div>
</div>
</div> 

