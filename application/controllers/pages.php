<?php
class Pages extends CI_Controller {
	public function __construct() 
        { 
            parent::__construct();
            $this->load->helper('url'); 
        } 

        public function view($page = 'home')
        {
					 if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
				{
						// Whoops, we don't have a page for that!
						show_404();
				}

				$data['title'] = ucfirst($page); // Capitalize the first letter

				$this->load->view('templates/hearder', $data);
				$this->load->view('pages/'.$page, $data);
				$this->load->view('templates/footer', $data);

			
        }
		
		public function views($page='about')
		{
			 if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
				{
						// Whoops, we don't have a page for that!
						show_404();
				}
			$this->load->views('templates/hearder',$data);
			$this->load->views('page/',$data);
			$this->load->views('templates/footer',$data);
		}
public function cart()
{
	$this->load->library('cart');
}		
}
?>